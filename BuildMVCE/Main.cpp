#include <cstdio>

#include "imgui/imgui.h"
#include "cimgui.h"
#pragma comment(lib, "cimgui.lib")

#include "SDL.h"
#undef main
#pragma comment(lib, "SDL2-staticd.lib")
#pragma comment(lib, "winmm.lib")
#pragma comment(lib, "imm32.lib")
#pragma comment(lib, "version.lib")

// VC Runtime
//#pragma comment(lib, "ucrtd.lib")
//#pragma comment(lib, "vcruntimed.lib")

// MFC Deps
//#pragma comment(lib, "libvcruntimed.lib")
//#pragma comment(lib, "libucrtd.lib")

extern "C" __declspec(dllexport) void main()
{
    SDL_Init(0);
    igCreateContext(0);
    printf("OK");
}